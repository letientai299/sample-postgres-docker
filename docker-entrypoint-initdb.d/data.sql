CREATE TABLE BOOK (
	id serial,
	title varchar,
	author varchar
);

insert into book(title, author) values ('The art of computer programming', 'Donald Knuth');
insert into book(title, author) values ('Effective Java', 'Joshua Bloch');
