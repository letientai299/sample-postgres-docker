FROM postgres:9.6.3-alpine

# Copy the database initialize script:
# Contents of /docker-entrypoint-initdb.d are run on db startup
ADD  docker-entrypoint-initdb.d/ /docker-entrypoint-initdb.d/

# Default database name
ENV POSTGRES_DB=postgres
